from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, FileResponse
from .forms import DownloadLinkForm
import requests
import random
import string
import os, glob
from django.conf import settings
from clint.textui import progress
import time
import json
import numpy as np

import progressbar
import urllib

from background_task import background
from django.core import management
import pika
import math

# Create your views here.
PATH_TO_UPLOADED_FILES_FOLDER = os.path.join(settings.BASE_DIR, 'uploaded-files', 'uploads/')
ROUTING_KEY = '16ha06nif88a43gung44'
URL_INFRALABS = 'http://localhost:8000/uploaded-files/uploads/'

def index(request):
    # cleaningUp()
    if request.method == 'POST':
        form = DownloadLinkForm(request.POST)
        if form.is_valid():
            download_url = form.cleaned_data['url']
            download_file_from_url(download_url)
            # controller_download(download_url)
            # publish_to_RabbitMQ_host(ROUTING_KEY, dictionaryStandardizedMessage(True, 'proses download dari target url', 200, {'download_url': 'makansoto'}))
            return render(request, 'core/progress.html', {'routing_key':ROUTING_KEY})
    else:
        form = DownloadLinkForm()
    return render(request, 'core/index.html', {'form': form})



@background(schedule=1)
def download_file_from_url(url, n_chunk=1):
    # get file_size
    req = urllib.request.Request(url, method='HEAD')
    f = urllib.request.urlopen(req)
    file_size = int(f.headers['Content-Length'])
    file_name = get_random_alphaNumeric_string() + "." + getExtension(f.headers['content-type'])
    r = requests.get(url, stream=True)
    block_size = 1024
    total = 0
    with open(PATH_TO_UPLOADED_FILES_FOLDER+file_name, 'wb') as f:
        for i, chunk in enumerate(r.iter_content(chunk_size=n_chunk * block_size)):
            f.write(chunk)
            total += len(chunk)
            percentage = math.floor(total/file_size * 100)
            publish_to_RabbitMQ_host(ROUTING_KEY, dictionaryStandardizedMessage(True, 'proses download dari target url', 200, {'percentage': percentage}))
            # Add a little sleep so you can see the bar progress
            time.sleep(0.015)
    publish_to_RabbitMQ_host(ROUTING_KEY, dictionaryStandardizedMessage(True, 'proses download dari target url', 200, {'linkdownload': URL_INFRALABS+file_name}))
    return

def get_random_alphaNumeric_string(stringLength=8):
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join((random.choice(lettersAndDigits) for i in range(stringLength)))

def getExtension(mimeType):
    return mimeType.split("/")[-1]

def publish_to_RabbitMQ_host(routing_key, message):
    routing_key = str(routing_key)
    credentials = pika.PlainCredentials('0806444524', '0806444524')
    parameters = pika.ConnectionParameters('152.118.148.95', 5672, '/0806444524', credentials)


    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    channel.exchange_declare(exchange='1606884344', exchange_type='direct')
    
    channel.queue_declare(queue=routing_key)

    channel.basic_publish(exchange='1606884344', routing_key=routing_key, body=json.dumps(message))
    

    connection.close()

def dictionaryStandardizedMessage(is_success, message, status_code, data):
    return {
        'success': is_success,
        'message': message,
        'status_code': status_code,
        'data': data
    }


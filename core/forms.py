from django import forms

class DownloadLinkForm(forms.Form):
    url = forms.CharField()